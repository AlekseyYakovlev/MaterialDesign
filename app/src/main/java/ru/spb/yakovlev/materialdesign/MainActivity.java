package ru.spb.yakovlev.materialdesign;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    MainActivityViewModel model;
    private ColorStateList colorStateList1;
    private ColorStateList colorStateList2;

    @BindView(R.id.welcome_text)
    TextView textView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @OnClick(R.id.fab)
    void fabClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        model = ViewModelProviders.of(this).get(MainActivityViewModel.class);


        setFields();

        initUI();
    }

    @SuppressWarnings("deprecation")
    private void setFields() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            colorStateList1 = getResources().getColorStateList(R.color.nav_veiw_colors_set_1, getTheme());
            colorStateList2 = getResources().getColorStateList(R.color.nav_veiw_colors_set_2, getTheme());

        } else {
            colorStateList1 = getResources().getColorStateList(R.color.nav_veiw_colors_set_1);
            colorStateList2 = getResources().getColorStateList(R.color.nav_veiw_colors_set_2);
        }
    }

    private void initUI() {
        setSupportActionBar(toolbar);
        initDrawer();
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(getOnNavigationItemSelectedListener());
    }

    @NonNull
    private NavigationView.OnNavigationItemSelectedListener getOnNavigationItemSelectedListener() {
        return item -> {
            switch (item.getItemId()) {
                case R.id.nav_camera:
                    showScrollingActivity();
                case R.id.nav_gallery:
                case R.id.nav_slideshow:
                case R.id.nav_manage:
                case R.id.nav_share:
                case R.id.nav_send:
                    showMsg(item.getTitle());
                    break;
                case R.id.col_1:
                    changeNavViewColors(1);
                    showMsg(item.getTitle());
                    break;
                case R.id.col_2:
                    changeNavViewColors(2);
                    showMsg(item.getTitle());
                    break;
            }

            drawer.closeDrawer(GravityCompat.START);
            return true;
        };
    }

    private void showScrollingActivity() {
        startActivity(new Intent(this, ScrollingActivity.class));
    }

    private void changeNavViewColors(int num) {
        navigationView.setItemIconTintList((num == 1) ? colorStateList1 : colorStateList2);
        navigationView.setItemTextColor((num == 1) ? colorStateList1 : colorStateList2);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    private void updateUI() {
        textView.setText(model.getWelcomeText());
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void showMsg(CharSequence string) {
        model.setWelcomeText(string);
        updateUI();
        Snackbar.make(textView, string, Snackbar.LENGTH_LONG).show();
    }
}
