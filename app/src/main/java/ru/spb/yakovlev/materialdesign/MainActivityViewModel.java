package ru.spb.yakovlev.materialdesign;

import android.arch.lifecycle.ViewModel;

public class MainActivityViewModel extends ViewModel {
    private CharSequence welcomeText = "Welcome!";


    public CharSequence getWelcomeText() {
        return welcomeText;
    }

    public void setWelcomeText(CharSequence welcomeText) {
        this.welcomeText = welcomeText;
    }
}
